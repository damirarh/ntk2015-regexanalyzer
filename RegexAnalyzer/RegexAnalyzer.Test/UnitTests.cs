﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestHelper;
using RegexAnalyzer;

namespace RegexAnalyzer.Test
{
    [TestClass]
    public class UnitTest : CodeFixVerifier
    {

        //No diagnostics expected to show up
        [TestMethod]
        public void TestMethod1()
        {
            var test = @"
    using System.Text.RegularExpressions;

    namespace RegExSample
    {
        public class Class1
        {
            public void Foo()
            {
                Regex.Match("""", """");
            }
        }
    }";

            VerifyCSharpDiagnostic(test);
        }

        //Diagnostic and CodeFix both triggered and checked for
        [TestMethod]
        public void TestMethod2()
        {
            var test = @"
    using System.Text.RegularExpressions;

    namespace RegExSample
    {
        public class Class1
        {
            public void Foo()
            {
                Regex.Match("""", ""["");
            }
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = RegexAnalyzerAnalyzer.DiagnosticId,
                Message = String.Format("Regular expression is invalid: {0}", @"parsing ""["" - Unterminated [] set."),
                Severity = DiagnosticSeverity.Error,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 10, 33)
                        }
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new RegexAnalyzerAnalyzer();
        }
    }
}